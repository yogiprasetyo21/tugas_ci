    
        			<center><b>INPUT DATA KARYAWAN</b><br></center>
                </div>
            </div>
   <center><div style="color: red"><?= validation_errors(); ?></div></center>

    <form action="<?=base_url()?>karyawan/input" method="post">
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" maxlength="20">
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_lengkap" id="nama_lengkap" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td><input type="text" name="tempat_lahir" id="tempat_lahir" /></td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <select name="jenis_kelamin" id="jenis_kelamin">
        <option value="L">Laki-Laki</option>
        <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
   	 <select name="tgl" id="tgl">
     <?php
     	for($tgl=1;$tgl<=31;$tgl++){
	 ?>
     	<option value="<?=$tgl;?>"><?=$tgl;?></option>
     <?php
		}
	 ?>
     </select>
      <select name="bln" id="bln">
      <?php
       $bulan_n = array('Januari','Februari','Maret','April',
	   					'Mei','Juni','Juli','Agustus','September',
						'Oktober','November','Desember');
		for($bln=0;$bln<12;$bln++){
	  ?>
      <option value="<?=$bln+1;?>">
      		<?=$bulan_n[$bln];?> </option>
      <?php
		}
	  ?>
      </select>
      <select name="thn" id="thn">
      <?php
      	for($thn = date('Y')-60;$thn <= date('Y')-15;$thn++){
	  ?>
      	<option value="<?=$thn;?>"><?=$thn;?></option>
      <?php
		}
	  ?>
      </select>

    </td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" /></td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"></textarea></td>
  </tr>
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      <select name="kode_jabatan" id="kode_jabatan">
      <?php foreach($data_jabatan as $data) {?>
      	<option value="<?= $data->kode_jabatan; ?>"><?= $data->nama_jabatan; ?></option>
      <?php } ?>
      </select>
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>Karyawan/listkaryawan">
    <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya"></a>
    </td>
  </tr>
</table>
</form>
</div>
</body>
</html>