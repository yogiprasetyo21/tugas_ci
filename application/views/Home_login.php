<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/login/style.css">
</head>
<body>
    <div class="konten" style="background-color:#0CF">
        <div class="kepala" style="background-color: #999" align="center">
            <h2 class="judul">LOGIN DI SINI</h2>
        </div>
        <div class="artikel">
            <form action="<?=base_url();?>auth/login" method="post">
                <div class="grup">
                    <label for="username">Username</label>
                <input type="text" name="username" id="username" placeholder="Username">
                </div>
                <div class="grup">
                    <label for="password">Password</label>
              <input type="password" name="password" id="password" placeholder="password">
                </div>
                <div class="grup" style="background-color: #F00">
                    <input type="submit" value="Sign In">
                </div>
                <div class="grup">
                <?php
				if($this->session->flashdata('info') == true){
					echo $this->session->flashdata('info');
				}
				?>
				</div>
            </form>
        </div>
    </div>
</body>
</html>